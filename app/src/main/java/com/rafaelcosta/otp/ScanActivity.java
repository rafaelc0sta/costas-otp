package com.rafaelcosta.otp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.rafaelcosta.otp.googlecode.HexEncoding;
import com.rafaelcosta.otp.utils.Base32;
import com.rafaelcosta.otp.utils.IntentIntegrator;
import com.rafaelcosta.otp.utils.IntentResult;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by rafael_costa on 3/6/14.
 */
public class ScanActivity extends Activity {

    TextView textView_otpSecret;
    TextView textView_accountName;
    TextView textView_serviceName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        textView_otpSecret = (TextView)findViewById(R.id.activityScan_editText_otpSecret);
        textView_accountName = (TextView)findViewById(R.id.activityScan_editText_accountName);
        textView_serviceName = (TextView)findViewById(R.id.activityScan_editText_serviceName);


        Button button_scan = (Button)findViewById(R.id.activityScan_button_scan);
        Button button_confirm = (Button)findViewById(R.id.activityScan_button_confirm);
        Button button_cancel = (Button)findViewById(R.id.activityScan_button_cancel);

        final Activity thisContext = this;
        button_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentIntegrator integrator = new IntentIntegrator(thisContext);
                integrator.initiateScan();
            }
        });

        button_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean shouldReturn = false;

                textView_accountName.setText(textView_accountName.getText().toString().trim());
                textView_otpSecret.setText(textView_otpSecret.getText().toString().trim());

                if (textView_otpSecret.getText().toString().length() == 0) {
                    textView_otpSecret.setError("Invalid OTP secret");
                    shouldReturn = true;
                }

                if (textView_otpSecret.getText().toString().length() != 16 && textView_otpSecret.getText().toString().length() != 32) {
                    textView_otpSecret.setError("Invalid OTP secret");
                    shouldReturn = true;
                }

                if (textView_accountName.getText().toString().length() == 0) {
                    textView_accountName.setError("Invalid Account name");
                    shouldReturn = true;
                }

                if (shouldReturn) return;

                ArrayList<HashMap<String, String>> entries = loadEntries();


                for (HashMap<String, String> entry : entries) {
                    if (entry.get("secret").equals(textView_otpSecret.getText().toString())) {
                        Toast.makeText(getApplicationContext(), "OTP entry already exists!", Toast.LENGTH_LONG).show();

                        finish();
                        return;
                    }
                    if (entry.get("account").equals(textView_accountName.getText().toString())) {
                        Toast.makeText(getApplicationContext(), "Username already exists!", Toast.LENGTH_LONG).show();
                        finish();
                        return;
                    }
                }



                //No cognates. No errors. Time to add!
                HashMap<String, String> otpEntry = new HashMap<String, String>();
                otpEntry.put("secret", textView_otpSecret.getText().toString());
                otpEntry.put("account", textView_accountName.getText().toString());
                otpEntry.put("service", textView_serviceName.getText().toString());
                entries.add(otpEntry);
                saveEntries(entries);
                finish();
            }
        });

        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



    }

    public ArrayList<HashMap<String, String>> loadEntries() {
        File keysFile = new File(getFilesDir(), "keys.totp");

        if (!keysFile.exists()) {
            try {
                keysFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            FileInputStream stream = new FileInputStream(keysFile);
            ObjectInputStream oStream = new ObjectInputStream(stream);

            return (ArrayList<HashMap<String, String>>)oStream.readObject();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return new ArrayList<HashMap<String, String>>();
    }


    public void saveEntries (ArrayList<HashMap<String, String>> entries) {
        File keysFile = new File(getFilesDir(), "keys.totp");

        if (!keysFile.exists()) {
            try {
                keysFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            FileOutputStream stream = new FileOutputStream(keysFile);
            ObjectOutputStream oStream = new ObjectOutputStream(stream);

            oStream.writeObject(entries);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // otpauth://totp/App.net%20rafaelcosta?secret=3GPYZPWW2AXC6H6B
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);

        if (scanResult != null && scanResult.getContents() != null) {
            Uri uri = Uri.parse(scanResult.getContents());
            String userInfo = uri.getPath();
            String secret = uri.getQueryParameter("secret");

            userInfo = userInfo.replaceFirst("/", "");

            String serviceName = null;
            String username = null;
            if (userInfo.contains(" ")) {
               serviceName = userInfo.split(" ")[0];
               username = userInfo.split(" ")[1];
            } else if (userInfo.contains(":")) {
                serviceName = userInfo.split(":")[0];
                username = userInfo.split(":")[1];
            } else {
                serviceName = "";
                username = userInfo;
            }

            if (secret.length() < 16) {
                Toast.makeText(getApplicationContext(), "Warning: Insecure OTP secret", Toast.LENGTH_LONG).show();
            }

            textView_otpSecret.setText(secret);
            textView_accountName.setText(username);
            textView_serviceName.setText(serviceName);
        }
        // else continue with any other code you need in the method
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
}
