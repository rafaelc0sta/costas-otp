package com.rafaelcosta.otp;

import android.app.Fragment;

/**
 * Created by rafael_costa on 3/13/14.
 */
public abstract class RFragment extends Fragment {
    public abstract void onBackPressed();
}
