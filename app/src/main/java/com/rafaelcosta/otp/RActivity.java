package com.rafaelcosta.otp;

import android.app.Activity;
import android.app.Fragment;

/**
 * Created by rafael_costa on 3/13/14.
 */
public abstract class RActivity extends Activity {
    public abstract void goToHomeFragment();
    public abstract void goToFragment(Fragment fragment);
}
