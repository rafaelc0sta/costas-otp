package com.rafaelcosta.otp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.rafaelcosta.otp.utils.IntentIntegrator;
import com.rafaelcosta.otp.utils.TotpClock;
import com.rafaelcosta.otp.utils.TotpCountdownTask;
import com.rafaelcosta.otp.utils.TotpCounter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;


public class MainActivity extends RActivity {

    Fragment homeFragment;
    int mMenuID = R.menu.main;
    Menu mMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        if (savedInstanceState == null) {
            homeFragment = new PlaceholderFragment();
            getFragmentManager().beginTransaction()
                    .replace(R.id.container, homeFragment)
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().findFragmentById(R.id.container).getClass().getSuperclass().equals(RFragment.class)) {
            ((RFragment)getFragmentManager().findFragmentById(R.id.container)).onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(mMenuID, menu);
        mMenu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_about) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("This app was developed by Rafael Costa <rafael@rafaelc.com.br>\r\n\r\nWARNING: This is a beta project. It may not be as safe as the final version.");
            builder.setTitle("About");
            builder.setNegativeButton("Ok", null);
            builder.show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void goToHomeFragment() {
        goToFragment(homeFragment);
    }

    @Override
    public void goToFragment(Fragment fragment) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.animator.slide_in, R.animator.slide_out);
        ft.replace(R.id.container, fragment);
        ft.commit();
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        ListView listView;

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);

            Button button_add = (Button)rootView.findViewById(R.id.main_button_add);
            button_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(getActivity(), ScanActivity.class);
                    startActivity(i);
                }
            });

            listView = (ListView)rootView.findViewById(R.id.listView);

            listView.setAdapter(new CustomAdapter(getActivity(), new ArrayList<HashMap<String, String>>()));

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    HashMap<String, String> entry = ((CustomAdapter)listView.getAdapter()).entries.get(i);
                    ((MainActivity)getActivity()).goToFragment(new GenerateFragment(entry));
                }
            });

            listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                    final int indexSelected = i;
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Are you sure you want to delete this OTP entry?\r\n\r\nWARNING: DELETING THE ENTRY *WILL NOT* DISABLE OTP ON THE ACCOUNT!!");
                    builder.setTitle("Confirm your action!");
                    builder.setNegativeButton("Cancel", null);
                    builder.setPositiveButton("Confirm!", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ((CustomAdapter)listView.getAdapter()).entries.remove(indexSelected);
                            saveEntries(((CustomAdapter)listView.getAdapter()).entries);
                            ((CustomAdapter)listView.getAdapter()).entries = loadEntries();
                            listView.invalidateViews();
                        }
                    });
                    builder.show();
                    return true;
                }
            });

            return rootView;
        }

        @Override
        public void onResume() {
            super.onResume();
            ((CustomAdapter)listView.getAdapter()).entries.clear();
            ((CustomAdapter)listView.getAdapter()).entries.addAll(loadEntries());
            listView.invalidateViews();
        }


        public ArrayList<HashMap<String, String>> loadEntries() {
            File keysFile = new File(getActivity().getFilesDir(), "keys.totp");

            if (!keysFile.exists()) {
                try {
                    keysFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            try {
                FileInputStream stream = new FileInputStream(keysFile);
                ObjectInputStream oStream = new ObjectInputStream(stream);

                return (ArrayList<HashMap<String, String>>)oStream.readObject();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            ArrayList<HashMap<String, String>> dict = new ArrayList<HashMap<String, String>>();
            return new ArrayList<HashMap<String, String>>();
        }


        public void saveEntries(ArrayList<HashMap<String, String>> entries) {
            File keysFile = new File(getActivity().getFilesDir(), "keys.totp");

            if (!keysFile.exists()) {
                try {
                    keysFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            try {
                FileOutputStream stream = new FileOutputStream(keysFile);
                ObjectOutputStream oStream = new ObjectOutputStream(stream);

                oStream.writeObject(entries);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//        }
    }

}
