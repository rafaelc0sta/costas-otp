package com.rafaelcosta.otp;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.rafaelcosta.otp.googlecode.GoogleAuthenticator;
import com.rafaelcosta.otp.utils.Base32;
import com.rafaelcosta.otp.utils.TotpClock;
import com.rafaelcosta.otp.utils.TotpCountdownTask;
import com.rafaelcosta.otp.utils.TotpCounter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by rafael_costa on 3/9/14.
 */
public class TestFragment extends Fragment {

    public TestFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_test, container, false);

        final TextView code = (TextView)rootView.findViewById(R.id.test_textView_code);
        final TextView time = (TextView)rootView.findViewById(R.id.test_textView_time);
        final TextView secret = (TextView)rootView.findViewById(R.id.test_textView_secret);

        TotpCounter counter = new TotpCounter(30);
        TotpClock clock = new TotpClock(getActivity());

        TotpCountdownTask task = new TotpCountdownTask(counter, clock, 100);
        task.setListener(new TotpCountdownTask.Listener() {

            @Override
            public void onTotpCountdown(long millisRemaining) {

            }

            @Override
            public void onTotpCounterValueChanged() {
                //Value has changed. Update counter.
                try {
                    TotpCounter counter = new TotpCounter(30);
                    code.setText(getCode("3GPYZPWW2AXC6H6B", counter));
                    secret.setText("3GPYZPWW2AXC6H6B");
                    long t = System.currentTimeMillis() / 1000;
                    time.setText("" + counter.getValueAtTime(t));
                } catch (Base32.DecodingException e) {
                    e.printStackTrace();
                }
            }
        });
        task.run();

        return rootView;
    }

    //"3GPYZPWW2AXC6H6B"
        public String getCode(String key, TotpCounter counter) throws Base32.DecodingException {

            if (key.length() != 16) {
                return "";
            }


            long t = System.currentTimeMillis() / 1000;

            byte[] secret = Base32.decode(key);

            int code = GoogleAuthenticator.calculateCode(secret, counter.getValueAtTime(t));

            String codeString = "0000" + code;

            codeString = codeString.substring(codeString.length() - 6);

            return codeString;
        }
}
