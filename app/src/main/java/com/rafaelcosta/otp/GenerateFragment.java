package com.rafaelcosta.otp;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.rafaelcosta.otp.googlecode.GoogleAuthenticator;
import com.rafaelcosta.otp.utils.Base32;
import com.rafaelcosta.otp.utils.TotpClock;
import com.rafaelcosta.otp.utils.TotpCountdownTask;
import com.rafaelcosta.otp.utils.TotpCounter;

import java.util.Map;

/**
 * Created by rafael_costa on 3/11/14.
 */
public class GenerateFragment extends RFragment {

    TextView textView_code;
    TextView textView_account;
    TextView textView_service;
    ProgressBar progressBar;

    Map<String, String> mEntry;

    public GenerateFragment(Map<String, String> entry) {
        mEntry = entry;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_generate, container, false);

        textView_code = (TextView)rootView.findViewById(R.id.fragmentGenerate_textView_code);
        textView_account = (TextView)rootView.findViewById(R.id.fragmentGenerate_textView_account);
        textView_service = (TextView)rootView.findViewById(R.id.fragmentGenerate_textView_service);
        progressBar = (ProgressBar)rootView.findViewById(R.id.progressBar);

        progressBar.setMax(30*1000);

        textView_account.setText(mEntry.get("account"));
        textView_service.setText(mEntry.get("service"));

        TotpCounter counter = new TotpCounter(30);
        TotpClock clock = new TotpClock(getActivity());

        TotpCountdownTask task = new TotpCountdownTask(counter, clock, 100);
        task.setListener(new TotpCountdownTask.Listener() {

            @Override
            public void onTotpCountdown(long millisRemaining) {
                progressBar.setProgress((int)millisRemaining);

                float pProgress = ((float)progressBar.getProgress()/(float)progressBar.getMax());
                if (pProgress <= 0.25) {
                    textView_code.setTextColor(Color.rgb((int)(255*(1.0 - pProgress)), 0, 0));
                } else {
                    textView_code.setTextColor(Color.rgb(255, 255, 255));
                }
            }

            @Override
            public void onTotpCounterValueChanged() {
                //Value has changed. Update counter.
                textView_code.setText(OTPCodeGenerator.getCode(mEntry.get("secret")));
            }
        });
        task.run();

        return rootView;
    }

    public void onBackPressed() {
        if (getActivity().getClass().getSuperclass().equals(RActivity.class)) {
            ((RActivity)getActivity()).goToHomeFragment();
        }
    }

    //"3GPYZPWW2AXC6H6B"
//    public String getCode(String key, TotpCounter counter) throws Base32.DecodingException {
//
//        if (key.length() != 16) {
//            return "";
//        }
//
//
//        long t = System.currentTimeMillis() / 1000;
//
//        byte[] secret = Base32.decode(key);
//
//        int code = GoogleAuthenticator.calculateCode(secret, counter.getValueAtTime(t));
//
//        String codeString = "0000" + code;
//
//        codeString = codeString.substring(codeString.length() - 6);
//
//        return codeString;
    //}
}
