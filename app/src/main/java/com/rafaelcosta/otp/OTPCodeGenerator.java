package com.rafaelcosta.otp;

import com.rafaelcosta.otp.googlecode.GoogleAuthenticator;
import com.rafaelcosta.otp.utils.Base32;
import com.rafaelcosta.otp.utils.TotpCounter;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by rafael_costa on 3/6/14.
 */
public class OTPCodeGenerator {


    public static String getCode(String key) {

        TotpCounter counter = new TotpCounter(30);

        Date date = Calendar.getInstance().getTime();

        long t = System.currentTimeMillis() / 1000;

        byte[] secret = new byte[0];
        try {
            secret = Base32.decode(key);
        } catch (Base32.DecodingException e) {
            e.printStackTrace();
        }

        int code = GoogleAuthenticator.calculateCode(secret, counter.getValueAtTime(t));

        String codeString = "0000" + code;

        codeString = codeString.substring(codeString.length() - 6);

        return codeString;

    }

}
