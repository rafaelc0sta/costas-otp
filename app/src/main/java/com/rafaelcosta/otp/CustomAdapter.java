package com.rafaelcosta.otp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by rafael_costa on 3/6/14.
 */
public class CustomAdapter extends BaseAdapter {

    ArrayList<HashMap<String, String>> entries;

    private Context context;
    protected LayoutInflater inflater;

    public CustomAdapter(Context mContext, ArrayList<HashMap<String, String>> mEntries) {
        entries = mEntries;
        context = mContext;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return entries.size();
    }

    @Override
    public Object getItem(int i) {
        return entries.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.row_otp, viewGroup, false);
        }

        HashMap<String, String> entry = entries.get(i);

        TextView textView_service = (TextView)view.findViewById(R.id.row_textView_service);
        TextView textView_secret = (TextView)view.findViewById(R.id.row_textView_secret);

        textView_service.setText(entry.get("service"));
        String secret = entry.get("secret");
        textView_secret.setText("..." + secret.substring(secret.length() - 4).toUpperCase());

        return view;
    }
}
